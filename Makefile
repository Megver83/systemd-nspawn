PKG_NAME=systemd-nspawn

$(PKG_NAME): $(PKG_NAME).o
	$(CC) -v $@.c -o $@

install:
	install -D $(PKG_NAME) $(DESTDIR)/usr/bin/$(PKG_NAME)

uninstall:
	rm -f $(DESTDIR)/usr/bin/$(PKG_NAME)
	
clean:
	rm -f $(PKG_NAME).o $(PKG_NAME)
